import csv

import pandas as pd


# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    df = pd.read_csv('resources/Bloco1.csv', skiprows=1,
                     names=["ID_ORCAMENTO", "CREATEDIN", "CONCLUIDO", "VALOR", "VALOR_PAGAR", "VENCTO",
                            "DT_NOTIFICACAO_AUDFINAL", "AUDITORIA_FINAL"])
    grouped = df.groupby('ID_ORCAMENTO')
    rows = []
    for name, group in grouped:
        selected = grouped.get_group(name)

        for row_index, row in group.iterrows():
            idOrcamento = row['ID_ORCAMENTO']
            dataCriacao = row['CREATEDIN']
            dataConclusao = row['CONCLUIDO']
            valorPago = row['VALOR']
            valorPagar = row['VALOR_PAGAR']
            dataVencimento = row['VENCTO']
            dataNotificacaoAuditoriaFinal = row['DT_NOTIFICACAO_AUDFINAL']
            dataAuditoriaFinal = row['AUDITORIA_FINAL']

            if valorPago > valorPagar:
                situacao = "SALDO POSITIVO"
            elif valorPago < valorPagar:
                situacao = "SALDO NEGATIVO"
            else:
                situacao = "OK"

            rows.append([idOrcamento, dataCriacao, dataConclusao, valorPago, valorPagar, dataVencimento, dataNotificacaoAuditoriaFinal, dataAuditoriaFinal, situacao])

    pd.DataFrame([*rows], columns=["ID_ORCAMENTO","CREATEDIN","CONCLUIDO","VALOR","VALOR_PAGAR","VENCTO","DT_NOTIFICACAO_AUDFINAL","AUDITORIA_FINAL", "SITUACAO"]).to_csv("resultado.csv")


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
